package oidc

import (
	"context"

	"github.com/coreos/go-oidc"
)

type Provider struct {
	Config *oidc.Provider
	KeySet oidc.KeySet
}

// NewRemoteProvider create provider with issuer url
func NewRemoteProvider(ctx context.Context, issuer string) (_ *Provider, err error) {
	config, err := oidc.NewProvider(ctx, issuer)
	if err != nil {
		return
	}
	var claim struct {
		JwksUri string `json:"jwks_uri"`
	}
	if err = config.Claims(&claim); err != nil {
		return
	}
	keySet := oidc.NewRemoteKeySet(ctx, claim.JwksUri)
	return &Provider{
		Config: config,
		KeySet: keySet,
	}, nil
}
