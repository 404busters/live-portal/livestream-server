package oidc

import (
	"context"
	"testing"
)

func TestNewRemoteProvider(t *testing.T) {
	issuer := "https://accounts.google.com"
	provider, err := NewRemoteProvider(context.Background(), issuer)
	if err != nil {
		t.Error(err)
		return
	}

	if provider.KeySet == nil {
		t.Error("key set is empty")
		return
	}

	if provider.Config == nil {
		t.Error("config is empty")
		return
	}
}
